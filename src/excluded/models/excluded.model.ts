import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Excluded {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  objectID: string;
}
