import { Test, TestingModule } from '@nestjs/testing';
import { ExcludedService } from './excluded.service';

describe('ExcludedService', () => {
  let service: ExcludedService;

  const excludedNews = {
    id: Date.now(),
    objectID: `${Date.now()}`,
  };

  const mockExcludedRepository = {
    find: jest.fn(() => [excludedNews]),
    findOne: jest.fn((objectID: string) => excludedNews),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ExcludedService,
        {
          provide: 'ExcludedRepository',
          useValue: mockExcludedRepository,
        },
      ],
    }).compile();

    service = module.get<ExcludedService>(ExcludedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should return the excluded news', () => {
    expect(service.getExcludedNews()).toEqual([excludedNews]);

    expect(mockExcludedRepository.find).toBeCalled();
  });

  it('Should insert an excluded news', () => {
    service.excludeNews(excludedNews.objectID).then((response) => {
      expect(response).toEqual(excludedNews);

      expect(mockExcludedRepository.findOne).toBeCalledWith({
        objectID: excludedNews.objectID,
      });
    });
  });
});
