import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Excluded } from './models/excluded.model';

@Injectable()
export class ExcludedService {
  constructor(
    @InjectRepository(Excluded)
    private excludedRepository: Repository<Excluded>,
  ) {}

  getExcludedNews(): Promise<Excluded[]> {
    return this.excludedRepository.find();
  }

  async excludeNews(objectID: string): Promise<Excluded> {
    const excluded = await this.excludedRepository.findOne({ objectID });

    if (excluded) throw new Error('NEWS already excluded');

    return this.excludedRepository.save({ objectID });
  }
}
