import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExcludedService } from './excluded.service';
import { Excluded } from './models/excluded.model';

@Module({
  imports: [TypeOrmModule.forFeature([Excluded])],
  providers: [ExcludedService],
  exports: [ExcludedService],
})
export class ExcludedModule {}
