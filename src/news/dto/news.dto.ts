import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDate,
  IsNumber,
  IsPositive,
  IsString,
} from 'class-validator';

export class NewsDTO {
  @ApiProperty({
    type: Number,
    description: `News's id.`,
  })
  @IsNumber()
  @IsPositive()
  id: number;

  @ApiProperty({
    type: Date,
    description: `News Date.`,
  })
  @IsDate()
  created_at: Date;

  @ApiProperty({
    type: String,
    description: `Title of the news.`,
  })
  @IsDate()
  title: string;

  @ApiProperty({
    type: String,
    description: `News's url.`,
  })
  @IsString()
  url: string;

  @ApiProperty({
    type: String,
    description: `Author of the news.`,
  })
  @IsString()
  author: string;

  @ApiProperty({
    type: Number,
    description: `Points of the news.`,
  })
  @IsNumber()
  @IsPositive()
  points: number;

  @ApiProperty({
    type: String,
    description: `Text of the story.`,
  })
  @IsString()
  story_text: string;

  @ApiProperty({
    type: String,
    description: `Comment Text.`,
  })
  @IsString()
  comment_text: string;

  @ApiProperty({
    type: Number,
    description: `Quantity of comments.`,
  })
  @IsNumber()
  @IsPositive()
  num_comments: number;

  @ApiProperty({
    type: Number,
    description: `Id of the story.`,
  })
  @IsNumber()
  @IsPositive()
  story_id: number;

  @ApiProperty({
    type: String,
    description: `Title of the Story.`,
  })
  @IsString()
  story_title: string;

  @ApiProperty({
    type: String,
    description: `Story's url.`,
  })
  @IsString()
  story_url: string;

  @ApiProperty({
    type: Number,
    description: `Id of the parent.`,
  })
  @IsString()
  parent_id: number;

  @ApiProperty({
    type: Number,
    description: `Created at i.`,
  })
  @IsNumber()
  @IsPositive()
  created_at_i: number;

  @ApiProperty({
    type: [String],
    description: `Tags of the news.`,
  })
  @IsArray()
  @IsString()
  _tags: string[];

  @ApiProperty({
    type: String,
    description: `Object ID.`,
  })
  @IsString()
  objectID: string;
}
