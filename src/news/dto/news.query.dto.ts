import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class NewsQueryDTO {
  @ApiPropertyOptional({
    description: 'Number of the page that the user is retrieving.',
  })
  @IsOptional()
  @IsString()
  page?: number;

  @ApiPropertyOptional({
    description: 'Option to filter items by Author field.',
  })
  @IsOptional()
  @IsString()
  author?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by _tags field.',
  })
  @IsOptional()
  @IsString()
  _tags?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by Month field.',
  })
  @IsOptional()
  @IsString()
  month?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by Title field.',
  })
  @IsOptional()
  @IsString()
  story_title?: string;
}
