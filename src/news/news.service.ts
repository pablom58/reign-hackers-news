import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { News } from './models/news.model';
import { NewsQueryDTO } from './dto/news.query.dto';
import { ExcludedService } from '../excluded/excluded.service';
import { Excluded } from '../excluded/models/excluded.model';

@Injectable()
export class NewsService {
  private MOTHS = [
    'JANUARY',
    'FEBRARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];

  constructor(
    private httpService: HttpService,
    private exludedService: ExcludedService,

    @InjectRepository(News)
    private newsRepository: Repository<News>,
  ) {}

  private saveNews(news: News[]): Promise<News[]> {
    return this.newsRepository.save(news);
  }

  async getNewsFromApi(): Promise<News[]> {
    const response = await firstValueFrom(
      this.httpService.get(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      ),
    );

    const excludedNews = (await this.exludedService.getExcludedNews()).map(
      (excluded: Excluded) => excluded.objectID,
    );

    const hitsToSave = response.data.hits.filter(
      (hit) => !excludedNews.includes(hit.objectID),
    );

    return this.saveNews(hitsToSave);
  }

  async getNews(filters: NewsQueryDTO): Promise<News[]> {
    const { author, _tags, story_title, month, page } = filters;

    const query = this.newsRepository
      .createQueryBuilder('news')
      .select('news.*');

    if (author) query.where('news.author = :author', { author });

    if (_tags)
      query.andWhere('news._tags @> Array[:..._tags]', {
        _tags: _tags.split(','),
      });

    if (story_title)
      query.andWhere('news.story_title = :story_title', { story_title });

    if (month)
      query.andWhere('EXTRACT(MONTH FROM news.created_at) = :moth_number', {
        moth_number: this.MOTHS.indexOf(month.toUpperCase()) + 1,
      });

    return query
      .take(5)
      .skip((page ? page - 1 : 0) * 5)
      .getRawMany();
  }

  async deleteNews(id: number): Promise<News> {
    const newsToExclude = await this.newsRepository.findOne({ id });

    if (!newsToExclude) throw new Error('News not found');

    await this.exludedService.excludeNews(newsToExclude.objectID);

    return this.newsRepository.remove(newsToExclude);
  }
}
