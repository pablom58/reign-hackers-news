import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ExcludedService } from '../excluded/excluded.service';
import { NewsDTO } from './dto/news.dto';
import { NewsQueryDTO } from './dto/news.query.dto';
import { News } from './models/news.model';
import { NewsService } from './news.service';

describe('NewsService', () => {
  let service: NewsService;

  const query: NewsQueryDTO = {};

  const news: NewsDTO = {
    id: 1,
    created_at: new Date('2021-10-02T15:28:39.000Z'),
    title: null,
    url: null,
    author: 'manoovau',
    points: null,
    story_text: null,
    comment_text:
      'Location: Berlin\nRemote: Yes\nWilling to relocate: No\nTechnologies: React, JS, Typescript, HTML, CSS, NodeJS, Git\nRésumé&#x2F;CV: www.linkedin.com&#x2F;in&#x2F;manoovau&#x2F;\nEmail: m-noel@hotmail.es',
    num_comments: null,
    story_id: 28719317,
    story_title: 'Ask HN: Who wants to be hired? (October 2021)',
    story_url: null,
    parent_id: 28719317,
    created_at_i: 1633188519,
    _tags: ['comment', 'author_manoovau', 'story_28719317'],
    objectID: '28729353',
  };

  const excludedNews = {
    id: Date.now(),
    objectID: `${Date.now()}`,
  };

  const mockExcludedService = {
    getExcludedNews: jest.fn(() => [excludedNews]),
    excludeNews: jest.fn((objectID: string) => ({
      id: Date.now(),
      objectID,
    })),
  };

  const mockNewsRepository = {
    getRawMany: jest.fn(() => [news]),
    findOne: jest.fn((data: { id: number }) => news),
    remove: jest.fn((data: News) => news),
  };

  const mockHttpService = {
    get: jest.fn(() => [news]),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        HttpService,
        ExcludedService,
        {
          provide: 'NewsRepository',
          useValue: mockNewsRepository,
        },
      ],
    })
      .overrideProvider(ExcludedService)
      .useValue(mockExcludedService)
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should save hacker news from API', () => {
    service.getNewsFromApi().then((response) => {
      expect(response).toEqual([news]);
      expect(mockHttpService.get).toBeCalledWith(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      );
      expect(mockExcludedService.getExcludedNews).toBeCalled();
    });
  });

  it('Should get hacker news from Database', () => {
    service.getNews(query).then((response) => {
      expect(response).toEqual([news]);
      expect(mockNewsRepository.getRawMany).toBeCalled();
    });
  });

  it('Should delete hacker news from Database', () => {
    service.deleteNews(news.id).then((response) => {
      expect(response).toEqual(news);
      expect(mockNewsRepository.findOne).toBeCalledWith({ id: news.id });
      expect(mockExcludedService.excludeNews).toBeCalledWith(news.objectID);
      expect(mockNewsRepository.remove).toBeCalledWith(news);
    });
  });
});
