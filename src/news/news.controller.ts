import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiAcceptedResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { NewsDTO } from './dto/news.dto';
import { NewsQueryDTO } from './dto/news.query.dto';
import { NewsService } from './news.service';
@ApiUnauthorizedResponse({
  description: 'Invalid access token',
})
@ApiBearerAuth('authorization')
@ApiTags('News')
@UseGuards(AuthGuard('jwt'))
@Controller('api/news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @ApiOperation({ summary: 'Get initial data.' })
  @ApiAcceptedResponse({
    description: 'Hacker News.',
    type: [NewsDTO],
  })
  @Get('init')
  getInitialNews(): Promise<NewsDTO[]> {
    return this.newsService.getNewsFromApi();
  }

  @ApiOperation({ summary: 'Get Hacker News.' })
  @ApiAcceptedResponse({
    description: 'Hacker News.',
    type: [NewsDTO],
  })
  @Get()
  getNews(@Query() query: NewsQueryDTO): Promise<NewsDTO[]> {
    return this.newsService.getNews(query);
  }

  @ApiOperation({ summary: 'Delete Hacker News.' })
  @ApiAcceptedResponse({
    description: 'Removed Hacker News.',
    type: NewsDTO,
  })
  @Delete(':id')
  deleteNews(@Param('id') id: number): Promise<NewsDTO> {
    return this.newsService.deleteNews(id);
  }
}
