import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class News {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  created_at: Date;

  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  url: string;

  @Column()
  author: string;

  @Column({ type: 'float', nullable: true, default: 0 })
  points: number;

  @Column({ type: 'text', nullable: true })
  story_text: string;

  @Column({ type: 'text' })
  comment_text: string;

  @Column({ type: 'float', nullable: true, default: 0 })
  num_comments: number;

  @Column({ type: 'float' })
  story_id: number;

  @Column()
  story_title: string;

  @Column({ nullable: true })
  story_url: string;

  @Column({ type: 'float' })
  parent_id: number;

  @Column({ type: 'float' })
  created_at_i: number;

  @Column('text', { array: true })
  _tags: string[];

  @Column({ unique: true })
  objectID: string;
}
