import { Test, TestingModule } from '@nestjs/testing';
import { NewsDTO } from './dto/news.dto';
import { NewsQueryDTO } from './dto/news.query.dto';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;

  const query: NewsQueryDTO = {};

  const news: NewsDTO = {
    id: 1,
    created_at: new Date('2021-10-02T15:28:39.000Z'),
    title: null,
    url: null,
    author: 'manoovau',
    points: null,
    story_text: null,
    comment_text:
      'Location: Berlin\nRemote: Yes\nWilling to relocate: No\nTechnologies: React, JS, Typescript, HTML, CSS, NodeJS, Git\nRésumé&#x2F;CV: www.linkedin.com&#x2F;in&#x2F;manoovau&#x2F;\nEmail: m-noel@hotmail.es',
    num_comments: null,
    story_id: 28719317,
    story_title: 'Ask HN: Who wants to be hired? (October 2021)',
    story_url: null,
    parent_id: 28719317,
    created_at_i: 1633188519,
    _tags: ['comment', 'author_manoovau', 'story_28719317'],
    objectID: '28729353',
  };

  const mockNewsService = {
    getNewsFromApi: jest.fn(() => [news]),
    getNews: jest.fn((query: NewsQueryDTO) => [news]),
    deleteNews: jest.fn((id: string) => news),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(mockNewsService)
      .compile();

    controller = module.get<NewsController>(NewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Should Init data from hacker news api', () => {
    expect(controller.getInitialNews()).toEqual([news]);

    expect(mockNewsService.getNewsFromApi).toBeCalled();
  });

  it('Should get data from database', () => {
    expect(controller.getNews(query)).toEqual([news]);

    expect(mockNewsService.getNews).toBeCalledWith(query);
  });

  it('Should delete an specific news', () => {
    expect(controller.deleteNews(news.id)).toEqual(news);

    expect(mockNewsService.deleteNews).toBeCalledWith(news.id);
  });
});
