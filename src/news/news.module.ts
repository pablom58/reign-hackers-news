import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';

import { News } from './models/news.model';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { ExcludedModule } from 'src/excluded/excluded.module';
import { NewsTasksService } from './news.tasks';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([News]), ExcludedModule],
  controllers: [NewsController],
  providers: [NewsService, NewsTasksService],
})
export class NewsModule {}
