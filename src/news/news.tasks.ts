import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { NewsService } from './news.service';

@Injectable()
export class NewsTasksService {
  private readonly logger = new Logger(NewsTasksService.name);

  constructor(private newsService: NewsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  getHackerNews() {
    this.logger.debug('Getting Data from API.');
    this.newsService.getNewsFromApi();
  }
}
