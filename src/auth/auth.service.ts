import { Injectable, NotFoundException } from '@nestjs/common';
import { UserDTO } from '../users/dto/user.dto';
import { User } from '../users/models/user.model';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { SignInDTO } from './dto/signin.dto';
import { JwtPayload } from './types';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  signUp(user: UserDTO): Promise<User> {
    return this.usersService.createUser(user);
  }

  async signIn(data: UserDTO): Promise<SignInDTO> {
    const user = await this.usersService.getUserByUsername(data.username);

    if (!user) throw new NotFoundException();

    if (!user.verifyPassword(data.password)) throw new Error('Bad Credentials');

    const payload: JwtPayload = { username: user.username };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
