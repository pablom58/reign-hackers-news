import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from '../users/dto/user.dto';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
  let controller: AuthController;

  const user: UserDTO = {
    username: 'Reign',
    password: 'Reign',
  };

  const mockAuthService = {
    signUp: jest.fn((dto: UserDTO) => ({
      ...dto,
      id: Date.now(),
    })),
    signIn: jest.fn((dto: UserDTO) => ({
      access_token: (Math.random() + 1).toString(36).substring(7),
    })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [AuthService],
    })
      .overrideProvider(AuthService)
      .useValue(mockAuthService)
      .compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Should Create an User', () => {
    expect(controller.singUp(user)).toEqual({
      id: expect.any(Number),
      username: 'Reign',
      password: expect.any(String),
    });

    expect(mockAuthService.signUp).toHaveBeenCalledWith(user);
  });

  it('Should Login an User', () => {
    expect(controller.signIn(user)).toEqual({
      access_token: expect.any(String),
    });

    expect(mockAuthService.signIn).toHaveBeenCalledWith(user);
  });
});
