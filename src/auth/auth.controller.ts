import { Body, Controller, Post } from '@nestjs/common';
import { ApiAcceptedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserDTO } from '../users/dto/user.dto';
import { AuthService } from './auth.service';
import { SignInDTO } from './dto/signin.dto';

@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Signup Users.' })
  @ApiAcceptedResponse({
    description: 'Created User.',
    type: UserDTO,
  })
  @Post('signup')
  singUp(@Body() user: UserDTO): Promise<UserDTO> {
    return this.authService.signUp(user);
  }

  @ApiOperation({ summary: 'Login User.' })
  @ApiAcceptedResponse({
    description: 'Generated access token.',
    type: SignInDTO,
  })
  @Post('signin')
  signIn(@Body() user: UserDTO): Promise<SignInDTO> {
    return this.authService.signIn(user);
  }
}
