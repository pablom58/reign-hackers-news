import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class SignInDTO {
  @ApiProperty({
    description: 'Access token that allows user to access private endpoints.',
  })
  @IsString()
  @IsNotEmpty()
  access_token: string;
}
