import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { UserDTO } from '../users/dto/user.dto';
import { JwtPayload } from './types';

describe('AuthService', () => {
  let service: AuthService;

  const user: UserDTO = {
    username: 'Reign',
    password: 'Reign',
  };

  const mockConfigService = {};

  const mockJwtService = {
    sign: jest.fn((paylaod: JwtPayload) =>
      (Math.random() + 1).toString(36).substring(7),
    ),
  };

  const mockUserService = {
    createUser: jest.fn((dto: UserDTO) => ({
      ...dto,
      id: Date.now(),
    })),
    getUserByUsername: jest.fn((username: string) => ({
      id: Date.now(),
      username,
      password: (Math.random() + 1).toString(36).substring(7),
    })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
        {
          provide: ConfigService,
          useValue: mockConfigService,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(mockUserService)
      .compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should Create an User', () => {
    expect(service.signUp(user)).toEqual({
      id: expect.any(Number),
      username: 'Reign',
      password: expect.any(String),
    });

    expect(mockUserService.createUser).toBeCalledWith(user);
  });

  it('Should Login an User', () => {
    service.signIn(user).then((response) => {
      expect(response).toEqual({
        access_token: expect.any(String),
      });

      expect(mockUserService.getUserByUsername).toBeCalledWith(user.username);

      expect(mockJwtService.sign).toBeCalledWith({ username: user.username });
    });
  });
});
