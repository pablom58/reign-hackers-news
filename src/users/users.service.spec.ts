import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from './dto/user.dto';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;

  const user: UserDTO = {
    username: 'Reign',
    password: 'Reign',
  };

  const mockUserRepository = {
    findOne: jest.fn((data: { username: string }) => ({
      ...user,
      id: Date.now(),
    })),
    save: jest.fn((data: UserDTO) => ({
      ...user,
      id: Date.now(),
    })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: 'UserRepository',
          useValue: mockUserRepository,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should create an user', () => {
    service.createUser(user).then((response) => {
      expect(response).toEqual({
        id: expect.any(Number),
        username: user.username,
        password: expect.any(String),
      });

      expect(mockUserRepository.findOne).toBeCalledWith({
        username: user.username,
      });

      expect(mockUserRepository.save).toBeCalledWith(user);
    });
  });

  it('Should retrieve an user by username field', () => {
    service.getUserByUsername(user.username).then((response) => {
      expect(response).toEqual({
        id: expect.any(Number),
        username: user.username,
        password: expect.any(String),
      });

      expect(mockUserRepository.findOne).toBeCalledWith({
        username: user.username,
      });
    });
  });
});
