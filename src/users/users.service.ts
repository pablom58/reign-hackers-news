import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDTO } from './dto/user.dto';
import { User } from './models/user.model';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async createUser(data: UserDTO): Promise<User> {
    const user = await this.userRepository.findOne({ username: data.username });

    if (user) throw new Error('User already exists');

    return this.userRepository.save({
      ...data,
      password: bcrypt.hashSync(data.password, bcrypt.genSaltSync()),
    });
  }

  async getUserByUsername(username: string): Promise<User> {
    return this.userRepository.findOne({ username });
  }
}
