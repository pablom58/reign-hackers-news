import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsOptional,
} from 'class-validator';

export class UserDTO {
  @ApiPropertyOptional({
    description: 'User ID.',
  })
  @IsOptional()
  @IsNumber()
  @IsPositive()
  id?: number;

  @ApiProperty({
    description: 'Unique username to indentify users.',
  })
  @IsString()
  @IsNotEmpty()
  username: string;

  @ApiProperty({
    description: 'User password.',
  })
  @IsString()
  @IsNotEmpty()
  password: string;
}
