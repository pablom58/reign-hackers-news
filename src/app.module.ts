import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { DatabaseModule } from './database/database.module';
import { ENVIRONMENTS } from './config/enviroments';
import { NewsModule } from './news/news.module';
import { ExcludedModule } from './excluded/excluded.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import validationSchema from './config/validationSchema';
import config from './config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      envFilePath: ENVIRONMENTS[process.env.NODE_ENV] || '.dev.env',
      load: [config],
      isGlobal: true,
      validationSchema,
    }),
    DatabaseModule,
    NewsModule,
    ExcludedModule,
    AuthModule,
    UsersModule,
  ],
})
export class AppModule {}
