<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">

## Description

This is an API for tech test in Reign about Backend Developer.

## Installation

```bash
$ yarn install
```
## Before Running the App

You will need to create the .dev.env and .prod.env files with the environment variables that are in the .example.env file.
## Running the app

```bash
$ docker-compose up
```

## Test

```bash
# unit tests
$ yarn test
```

## Endpoints

- API - [http://localhost:3000/api](http://localhost:3000/api)
- Swagger - [http://localhost:3000/api/docs](http://localhost:3000/api/docs)

## Advices
To access to the News endpoints you will need to create an User an make login in order to generate the Token to access to the other endpoints.

You will find all of these in the postman collection.

## Made By

Pablo Miguel Villamizar Sanchez.
